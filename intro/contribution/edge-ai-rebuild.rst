.. _contribution-edge-ai-rebuild:

Rebuild and augment Edge-AI tools for BeagleBoard.org Debian
############################################################

.. todo::

   * How to rebuild all the DSP and R5 code used
   * How to test it
   * How to extend it
